import React, { useState } from 'react'

import Login from './components/login'
import Register from './components/register'

import firebase from 'firebase'
import firebaseConfig from './firebase/firebaseConfig'

export const configureFirebase = firebase.initializeApp(firebaseConfig)

function App() {
    const [isUser, setIsUser] = useState(true)

    return (
        <div className="App">
            <div className="outer">
                <div className="inner">
                    {isUser ? (
                        <button onClick={() => setIsUser(false)}>
                            Not our user yet? Register now
                        </button>
                    ) : (
                        <button onClick={() => setIsUser(true)}>
                            Already our user? Login with your account
                        </button>
                    )}
                    {isUser ? <Login /> : <Register />}
                </div>
            </div>
        </div>
    )
}

export default App
