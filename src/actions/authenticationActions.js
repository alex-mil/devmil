import * as types from './index';

export function loginUser(values) {
    return {
        type: types.LOGIN_USER,
        values
    }
}

export function registerUser(values) {
    return {
        type: types.REGISTER_USER,
        values
    }
}