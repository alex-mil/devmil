import { render } from '@testing-library/react'
import App from '../App'
import { Provider } from 'react-redux'
import configureStore from '../store/configureStore'

const mockStore = configureStore

test('renders authorization screen', () => {
    const container = render(
        <Provider store={mockStore}>
            <App />
        </Provider>
    )
    expect(container).toBeTruthy()
})
