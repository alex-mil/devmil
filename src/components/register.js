import React from 'react'
import { connect } from 'react-redux'
import { useFormik } from 'formik'
import { registerUser } from '../actions/authenticationActions'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope, faKey } from '@fortawesome/free-solid-svg-icons'

function validate(values) {
    const errors = {}

    if (!values.email) {
        errors.email = 'Required'
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }

    if (!values.password) {
        errors.password = 'Required'
    } else if (values.password.length < 4) {
        errors.password = 'Must be 4 symbols or more'
    }

    return errors
}

function Register(props) {
    const formik = useFormik({
        initialValues: { email: '', password: '' },
        validate,
        onSubmit: (values) => {
            props.dispatchedRegisterUser(values)
        },
    })

    return (
        <div>
            <h1>REGISTER</h1>
            <form onSubmit={formik.handleSubmit}>
                <label htmlFor="email">
                    <FontAwesomeIcon icon={faEnvelope} />
                </label>
                <input
                    id="email"
                    name="email"
                    type="email"
                    placeholder="Email"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.email}
                />
                {formik.touched.email && formik.errors.email ? (
                    <div>{formik.errors.email}</div>
                ) : null}
                <br />

                <label htmlFor="password">
                    <FontAwesomeIcon icon={faKey} />
                </label>
                <input
                    id="password"
                    name="password"
                    type="password"
                    placeholder="Password"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.password}
                />
                {formik.touched.password && formik.errors.password ? (
                    <div>{formik.errors.password}</div>
                ) : null}
                <br />

                <button type="submit" disabled={formik.isSubmitting}>
                    Register
                </button>
            </form>
        </div>
    )
}

function mapDispatchToProps(dispatch) {
    return {
        dispatchedRegisterUser(values) {
            dispatch(registerUser(values))
        },
    }
}

export default connect(null, mapDispatchToProps)(Register)
