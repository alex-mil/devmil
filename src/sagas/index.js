import {all, put, call, takeLatest} from 'redux-saga/effects'
import { loginUserService, registerUserService } from "../services/authenticationService";
import * as types from '../actions';

export function* loginSaga(action) {
    try {
        const response = yield call(loginUserService, action.values);
        yield put({ type: types.LOGIN_USER_SUCCESS, response });
    }
    catch(error) {
        yield put({ type: types.LOGIN_USER_ERROR, error })
    }
}

export function* registerSaga(action) {
    try {
        const response = yield call(registerUserService, action.values);
        yield put({ type: types.REGISTER_USER_SUCCESS, response });
    }
    catch(error) {
        yield put({ type: types.REGISTER_USER_ERROR, error })
    }
}

export function* watchLoginSaga() {
    yield takeLatest(types.LOGIN_USER, loginSaga)
}

export function* watchRegisterSaga() {
    yield takeLatest(types.REGISTER_USER, registerSaga)
}

export default function* rootSaga() {
    yield all ([
        watchLoginSaga(),
        watchRegisterSaga()
    ])
}